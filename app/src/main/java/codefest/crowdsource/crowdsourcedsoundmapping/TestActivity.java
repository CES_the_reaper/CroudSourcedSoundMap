package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TestActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_testSound, btn_testGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btn_testSound = (Button) findViewById(R.id.btn_testSound);
        btn_testGPS = (Button) findViewById(R.id.btn_testGPS);

        btn_testGPS.setOnClickListener(this);
        btn_testSound.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = null;

        switch (v.getId())
        {
            case R.id.btn_testSound:
                i = new Intent(this,SoundActivity.class);
                break;
            case R.id.btn_testGPS:
                i = new Intent(this,LocationActivity.class);
                break;
        }

        if(i != null) startActivity(i);
    }
}
