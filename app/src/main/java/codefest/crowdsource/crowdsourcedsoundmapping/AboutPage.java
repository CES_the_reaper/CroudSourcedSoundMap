package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class AboutPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appBarLayout);
        setSupportActionBar(toolbar);

        // Sets up the read more button.
        Button termCondBtn = (Button) findViewById(R.id.Term_Button);
        termCondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchTermsAndConditionsPageActivity();
            }
        });
    }

    // Launches the terms and conditions page when called.
    public void LaunchTermsAndConditionsPageActivity()
    {
        Intent i = new Intent(this, TermsAndConditionsPage.class);
        startActivity(i);
    }
}
