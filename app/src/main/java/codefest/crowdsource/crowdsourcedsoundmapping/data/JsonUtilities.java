package codefest.crowdsource.crowdsourcedsoundmapping.data;

import com.google.gson.Gson;



/**
 * Created by smajors on 4/7/17.
 */
public class JsonUtilities {
    /**
     * Serializes a LocationDataObject into JSON
     * @param object The created and non-null LocationDataObject
     * @return JSON string representation of LocationDataObject
     */
    public static String serializeToJsonObject(LocationDataObject object) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(object);
        return jsonString;
    }

    /**
     * Deserializes a JSON string into a LocationDataObject
     * @param jsonString the JSON string representing the LocationDataObject
     * @return a LocationDataObject
     */
    public static LocationDataObject deserializeFromJsonObject(String jsonString) {
        Gson gson = new Gson();
        LocationDataObject lObject = gson.fromJson(jsonString, LocationDataObject.class);
        return lObject;
    }
}
