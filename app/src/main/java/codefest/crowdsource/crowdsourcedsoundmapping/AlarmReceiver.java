package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Quinn on 4/8/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    public static final int REQUESTCODE = 123;
    public static final String ACTION = ".alarm";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, RepeaterService.class);
        context.startService(i);
    }
}