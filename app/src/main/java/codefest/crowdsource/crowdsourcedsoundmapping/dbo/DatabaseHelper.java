package codefest.crowdsource.crowdsourcedsoundmapping.dbo;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by chrys on 4/8/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private ArrayList<String> creationStrings = new ArrayList<>(1);
    private static final String dbName = "CSSMA";

    public DatabaseHelper(Context context){
        super(context, dbName, null, 33);
    }
    public void addCreationString(String cString){
        creationStrings.add(cString);
    }

    public void onCreate(SQLiteDatabase db){
        /*for (int i = 0; i < creationStrings.size(); i++){
            db.execSQL(creationStrings.get(i));
        }*/
        db.execSQL("CREATE TABLE CSSMA_TEST(ID INTEGER PRIMARY KEY, Decibels FLOAT, Latitude FLOAT, Longitude FLOAT, Altitude FLOAT, Granularity FLOAT, Timestamp FLOAT);");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }
    public void insertIntoCSSMATest(HashMap<String,Double> data){
        if(data.containsKey("Decibels") &&
                data.containsKey("Latitude") &&
                data.containsKey("Longitude") &&
                data.containsKey("Altitude") &&
                data.containsKey("Granularity") &&
                data.containsKey("Timestamp")){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv=new ContentValues();
            cv.put("Timestamp", data.get("Timestamp"));
            cv.put("Decibels", data.get("Decibels"));
            cv.put("Longitude", data.get("Longitude"));
            cv.put("Latitude", data.get("Latitude"));
            cv.put("Altitude", data.get("Altitude"));
            cv.put("Granularity", data.get("Granularity"));
            db.insert("CSSMA_TEST", "Index", cv);
            Log.i("DBOPS.PATH",db.getPath());
            db.close();
        }
    }
    public ArrayList<HashMap<String,Double>> getFromCSSMATest(){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] usableElements = {"Decibels","Latitude","Longitude","Altitude","Granularity","Timestamp"};
        Cursor dataCursor = db.query("CSSMA_TEST",usableElements,null,null,null,null,"ID");
        ArrayList<HashMap<String,Double>> dataCache = new ArrayList<>(1);
        for(int i = 0; i < dataCursor.getCount(); i++){
            HashMap<String,Double> hm = new HashMap<>();
            hm.put("Timestamp", Double.parseDouble(dataCursor.getString(1)));
            hm.put("Decibels", Double.parseDouble(dataCursor.getString(2)));
            hm.put("Latitude", Double.parseDouble(dataCursor.getString(3)));
            hm.put("Longitude", Double.parseDouble(dataCursor.getString(4)));
            hm.put("Altitude", Double.parseDouble(dataCursor.getString(5)));
            hm.put("Granularity", Double.parseDouble(dataCursor.getString(6)));
            dataCache.add(hm);
        }
        return dataCache;
    }
}
